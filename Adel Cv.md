![image](My image.jpg)


# Adel Saltaji                                           

## Personal Information:
* Nationality: Syrian.
* Adress: Beirut.
* Mobile: 00961-71508311
* E-mail: saltajiadel@gmail.com

## Education:
* Damascus university, facuilty of economics                  
***
## Experience:
### Sanad team fo Development: Project mangaer:                      2015- present
* Planning and executing of the projects.
* writing proposals and concept notes fpr the projects.
* Monitoring and evaluating of the projects.
### GOPA-DERD "UNHCR outreach program": Outreach "team coordinator"
* Finding out the need of the community by holding foucs groups and making visit to social figuers in the local community.
* working with local communities to make initiatives based on their needs.
* Follow-up with more than 100 family and working to fulfill their need.
### Child right society:
* Planning activities for children Psychical support.
* Supervising activities implementation.
* Coordinator CRS & WHO vaccine project for 2 years in raw, in Damascus countryside.
* Responsible for vaccine campaigns planning.
* Supervising vaccine campaigns execution.
* Data analysis and Reporting.
* Coordinator CRS & UNICEF back to learning projects for 2 years in a raw.
* Responsible for planning of field awareness campaigns.
* Designing surveys and observation.
* Supervising the field execution and data collecting process.
* Data analysis and Reporting.
### Youth coordinator @ Nsj (UNHCR 2015 best world youth initiative)
* Planning and designing the training program.
* Coordinate training and activities.
* Reporting.
***
## Voluntary experience:
* War Child Holland/ Jafraa Foundation: I Deal program trainer in IDPs shelters.
2013
* Children’s psychosocial support training “I DEAL” in IDPs shelters
* Al Nada society: Supervisor of Uthman Bin Affan IDPs shelter. 
* SHABAB project: Training for youth in public schools in a "Bussines awarenes program".
***
## Trainings: 
* Organizational structure and strategic palnning.
* Sphere Project: Child Rights Society.
* Intermediate accounting “IA” : Al Tanmya group.
* PMP: Project Management Professional : Pyramid engineering Syria.
* Quality management system: Better business.
* I Deal children’s Psychosocial support : War Child Holland.
* Psychosocial support: Child Rights Society.
* Business experience Training of Trainers: SHABAB.
***
## Key skills:
* Team working and team leading gained throughout my experience as a
coordinator at CRS and a team member of projects department at Sanad for
Development.
* Communication skills improved while working with different segments on field.
* Organizing skills gained by working with data and preparing reports.
***
## Languages
* Arabic as a mother tongue.
* English as a very good user.
***
## Skills:
* Languages: English: Very good, Arabic:Mother tounge.
* Computer skills: MS.Office : Very good



